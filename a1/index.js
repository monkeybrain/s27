//WD078-27 Node.js Routing w/ HTTP Methods

const http = require('http');

const PORT = 4000;


http.createServer( function(request, response) {

	if (request.url === "/"){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to Booking System')
	}

	else if (request.url === "/profile"){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to your profile')
	}


	else if (request.url === "/courses" && request.method === "POST"){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Heres our available courses')
	}  

	else if (request.url === "/addcourse" && request.method === "POST"){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Add a course to our resources')
	} 
	
	else if (request.url === "/updatecourse" && request.method === "PUT"){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Update a course to our resources')
	} 
	else if (request.url === "/archivecourse" && request.method === "DELETE"){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Archive a course to our resources')
	} 

}).listen(PORT)

console.log(`Server is running at localhost:${PORT}`)